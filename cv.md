---
layout: resume
---

`2018 - Current`
__Oregon Health and Science University__ <br>
PhD: Computational Biomedicine

`2008 - 2013`
__Winona State University__ <br>
BS: Biochemistry

## Awards 

`2018-2019`
National Library of Medicine Fellow 

## Publications

**American Journal of Ophthalmology** `Accepted Feb 2019`
- <span style="color:#a0b1c1">*Projection-Resolved Optical Coherence Tomographic Angiography of Retinal Plexuses in Retinitis Pigmentosa*</span>

**Translational Vision Science and Technology**  `Accepted Jan 2019`  
- <span style="color:#a0b1c1">*Repeatability of Adaptive Optics Automated Cone Measurements in Subjects with Retinitis pigmentosa and Novel Metrics for Assessment of Image Quality*</span>

**Addiction Biology**  `Early Submission Nov 2018`
- <span style="color:#a0b1c1">*White-Matter Microstructure During Adolescence is Associated with Future Binge Drinking*</span>

**AJO Case Reports**  `March 2018`  
- <span style="color:#a0b1c1">*Longitudinal Ophthalmic Findings in a Child with Helsmoortel Van der Aa Syndrome*</span>

## Conferences

`May 2018`  
**Association for Research in Vision and Ophthalmology**
- <span style="color:#a0b1c1">*Projection-resolved Optical Coherence Tomography Angiography of Macular retinal circulations in Retinitis Pigmentosa and Usher Syndrome Type 1*</span>

`May 2018`  
**Association for Research in Vision and Ophthalmology**  
- <span style="color:#a0b1c1">*Intersession Repeatability of Flood-Illuminated Adaptive Optics in Retinitis Pigmentosa*</span>

`May 2017`  
**Association for Research in Vision and Ophthalmology**
- <span style="color:#a0b1c1">*Variations in Cone Density and Intensity with Off-Entry Adaptive Optics Imaging*</span>

`May 2016`  
**Association for Research in Vision and Ophthalmology**   
- <span style="color:#a0b1c1">*Correlating the Relationship Between Fundus Guided Perimetry and Flood-Illuminated Adaptive Optics Imaging*</span>

`April 2013`  
**Senior Research Seminar - Winona State University**  
- <span style="color:#a0b1c1">*An Analysis of the Ketamine Model of Schizophrenia*</span>

## Occupation

`Dec 2017 – Present`
__Developer / Engineer__, Oregon Health & Science University 

- Develop and maintain software for fMRI data processing and analyses
-	Systems Administration on Linux
-	Image processing and analysis (MATLAB, Python, R)
-	Oversight and management of image processing trainings

`Dec 2014 – dec 2017`
__Research Assistant II__, Oregon Health & Science University

-	Instrumental testing for subjects enrolled in clinical trials
-	Document, record, and process subject data, history, protocol variations, and encounters
-	Maintain training and certification as part of involvement in testing
-	Develop scripts in (MATLAB, Python, R) to process, interpret, and display data
-	GUI development (MATLAB, Python) to allow colleagues to use custom software for image processing
-	Independently pursue research projects involving Adaptive Optics Ophthalmic Imaging

`Dec 2013 – Dec 2014`
__Direct Support Professional__, Mentor Oregon

-	Assist individuals with disabilities in daily activities including meal preparation, eating, and hygiene
-	Administer and document medications including medical incidents and medication errors
-	Facilitate community involvement by accompanying individuals on outings in the community
-	Assist individuals in meeting and achieving individual service plans to enhance quality of life

`Aug 2012 – May 2013`
__Independent Student Researcher__, Winona State University

-	Maintenance of animal colony (200 mice (C57BL/6J))
-	Rodent surgery including brain removal and CNS dissection
-	Preparation of NMR samples for 31P NMR analysis
-	Grant writing, maintenance of a research budget, and ordering of materials and chemicals
-	Preparation of report materials and presentation of findings

<!-- ### Footer

Last updated: May 2013 -->


