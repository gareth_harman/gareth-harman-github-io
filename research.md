---
layout: page
title: Research
---

### Repeatability of Adaptive Optics Automated Cone Measurements in Subjects with Retinitis pigmentosa and Novel Metrics for Assessment of Image Quality
*Translational Vision Science & Technology*

**Authors:**
`Michael Gale`
`Gareth Harman`
`Jimmy Chen`
`Dr. Mark Pennesi`

**Background**
Adaptive optics retinal imaging captures the cone photoreceptors in vivo.  This modality allows for diagnostics such as cone count, cone density, and the ability to view alterations to the cone mosaic in retinal diseases. This paper examines novel metrics to better establish image quality, cone spacing, and cone location similarity in both healthy controls and those with [retinitis pigmentosa](https://nei.nih.gov/health/pigmentosa/pigmentosa_facts).

**Novel Metric: Cone Location Similarity**
There is an issue of misidentifying retinal debris in the diseased retina as cones due to the altered topography of this retinal layer. This metric seeks to establish cones that are captured by repeated imaging to establish a greater level of confidence in accurate cone detection. A brief of the explanation is provided below.


<!-- To INCLUDE on publication finalization
![Cone Location Similarity](images/cls_algorithm/cls_algorithm.png)-->


