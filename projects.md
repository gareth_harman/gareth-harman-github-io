---
layout: page
title: Projects
---

### <span style="color:blue">[Subreddit Title Sentiment Analysis](https://github.com/gareth-harman/subreddit_nlp)</span>: Parse and examine NLP diagnostics from subreddit titles

This tool allows users to retrieve the 'n' posts from a given subreddit's post titles in order to examine NLP metrics.  For example you can return a sentiment score for each title and plot the distribution of sentiment scores for all posts retrieved.  The user can also return the 'p' most common words or nouns from the retrieved posts.

`run_sub_nlp.sh` contains...

```sh
#!/bin/bash

sub=$1
n_posts=$2

python3 subreddit_nlp.py \
         -id my_id \
         -subreddit ${sub} \
         -n_posts ${n_posts} \
         -secret my_secret \
         -user_agent my_user_agent \
         -username my_username \
         -sort_by_top 1
```

**r/Gradschool**

```sh
./run_sub_nlp.sh gradschool 1000
  Scraping gradschool for 1000 posts...
    retrieving by top of all time
  Processing reddit data...
  Returning frequent words...
      ('school', 127)
      ('phd', 77)
      ('thesis', 66)
      ('students', 58)
      ('today', 52)
```

![](/images/sub_nlp/gradschool_sent.png)

**r/Anxiety**

```sh
./run_sub_nlp.sh anxiety 1000
  Scraping anxiety for 1000 posts...
    retrieving by top of all time
  Processing reddit data...
  Returning frequent words...
      ('anxiety', 385)
      ('day', 66)
      ('today', 65)
      ('people', 64)
      ('time', 59)
```

![](/images/sub_nlp/anxiety_sent.png)

**r/Aww**

```sh
./run_sub_nlp.sh aww 1000
  Scraping aww for 1000 posts...
    retrieving by top of all time
  Processing reddit data...
  Returning frequent words...
      ('cat', 95)
      ('dog', 94)
      ('today', 43)
      ('baby', 40)
      ('friend', 36)
```

![](/images/sub_nlp/aww_sent.png)


