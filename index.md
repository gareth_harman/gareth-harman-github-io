---
layout: page
excerpt: "About Me..."
---

![](images/surf.png)

My name is Gareth and I am a first year PhD student studying [Bioinformatics and Computational Biology](https://www.ohsu.edu/xd/education/schools/school-of-medicine/departments/clinical-departments/dmice/) at Oregon Health & Science University. I am currently a student in Dr. Bonnie Nagel's [Developmental Brain Imaging Lab](https://www.ohsu.edu/xd/education/schools/school-of-medicine/departments/clinical-departments/psychiatry/research/developmental-brain-imaging-lab/index.cfm) where I am interested in using high dimensional behavioral and neuroimaging data to build models to predict trait behavior leading to alcohol and substance abuse in adolescence.  

Prior to beginning graduate school I was a research assistant in the genetics department at the Casey Eye Institue. My work involved exploring methods to assess the quality and reliability of the photoreceptor mosaic as captured by `adaptive optics` imaging. Additionally, I worked as a developer and engineer in Dr. Bonnie Nagel's developmental neuroimaging lab where I helped to develop and maintain pipelines to process and interpret multi-modal neuroimaing data.  

**PhD Student:** Bioinformatics and Computational Biomedicine <br>
**Organization:** Oregon Health & Science University <br> <br>
**Research Interests**
- Neuroimaging
- Adolescent Mood disorders
- Substance use disorders
- Machine learning
- Adaptive Optics retinal imaging
- Automation and Metrics for Quality Control in Imaging

```python
#####################################################
# Efficiency definition
#####################################################

def efficient_graduate_student(coffee, food):
  while coffee != "full"
    coffee += 1 # cup
    
    if coffee >= 16: # ounces
      coffee = "full"
  
  if food == "eaten":
    venture = "out to lunch"
    
   return coffee, food
   
Gareth = efficient_graduate_student("empty", "eaten")

```
